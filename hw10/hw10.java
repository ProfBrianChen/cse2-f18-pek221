/////////////////////
//// Pierre Klur
//// 12/3/18
//// CSE 002 - 110: hw10 - Tic-tac-toe
// The purpose of this hw is to simulate
// a game of tic-tac-toe.

import java.util.Scanner; // import scanner

public class hw10 {

	public static void main(String[] args) {
		String [][] array = {
				{"1","2","3"}, 
				{"4","5","6"},
				{"7","8","9"}
		};
		boolean winner = false;
		int counter = 0;
		int position = 0; 
		
		while (winner == false) {
			print(array);
			System.out.println("Enter the position number you wish to mark");
			if (counter % 2 == 0) {
				// place an X
				position = getPosition(array);
				switch (position) {
				case 1:
					array [0][0] = "X";
					break;
				case 2:
					array [0][1] = "X";
					break;
				case 3: 
					array [0][2] = "X";
					break;
				case 4: 
					array [1][0] = "X";
					break;
				case 5: 
					array [1][1] = "X";
					break;
				case 6: 
					array [1][2] = "X";
					break;
				case 7:
					array [2][0] = "X";
					break;
				case 8:
					array [2][1] = "X";
					break;
				case 9: 
					array [2][2] = "X";
				}
			}
			else {
				// place a O
				position = getPosition(array);
				switch (position) {
				case 1:
					array [0][0] = "O";
					break;
				case 2:
					array [0][1] = "O";
					break;
				case 3: 
					array [0][2] = "O";
					break;
				case 4: 
					array [1][0] = "O";
					break;
				case 5: 
					array [1][1] = "O";
					break;
				case 6: 
					array [1][2] = "O";
					break;
				case 7:
					array [2][0] = "O";
					break;
				case 8:
					array [2][1] = "O";
					break;
				case 9: 
					array [2][2] = "O";
					break;
				}
				
			}
			counter++;
			winner = checkWinner(array);
			if (winner == true) {
				break;
			}
			winner = checkDraw(array);
		}
		
		print(array);
		System.out.println("The game is over");
	}
	
	public static void print(String [][]array) {
		for(int i = 0; i < array.length; i++) {
			for(int k = 0; k < array[i].length; k++) {
				System.out.print(array[i][k] + "  ");
			}
			System.out.println();
		}
	}

	public static int getPosition(String [][]array) {
		 Scanner myScanner = new Scanner(System.in);
		 int position = 0; 
		 boolean a = false;
		 while (a == false){  
			 a = myScanner.hasNextInt();
			 if (a == true){
				position = myScanner.nextInt();
				 if (position < 1 || position > 9) {
					 System.out.println("Error: the integer is out of range");
					 a = false;
					 continue;
				 }
				 
				 switch (position) {
					case 1:
						if (array [0][0] == "O" || array [0][0] == "X") {
							System.out.println("Error: that position has already been selected");
							a = false;
							continue;
						}
						break;
					case 2:
						if (array [0][1] == "O" || array [0][1] == "X") {
							System.out.println("Error: that position has already been selected");
							a = false;
							continue;
						}
						break;
					case 3: 
						if (array [0][2] == "O" || array [0][2] == "X") {
							System.out.println("Error: that position has already been selected");
							a = false;
							continue;
						}
						break;
					case 4: 
						if (array [1][0] == "O" || array [1][0] == "X") {
							System.out.println("Error: that position has already been selected");
							a = false;
							continue;
						}
						break;
					case 5: 
						if (array [1][1] == "O" || array [1][1] == "X") {
							System.out.println("Error: that position has already been selected");
							a = false;
							continue;
						}
						break;
					case 6: 
						if (array [1][2] == "O" || array [1][2] == "X") {
							System.out.println("Error: that position has already been selected");
							a = false;
							continue;
						}
						break;
					case 7:
						if (array [2][0] == "O" || array [2][0] == "X") {
							System.out.println("Error: that position has already been selected");
							a = false;
							continue;
						}
						break;
					case 8:
						if (array [2][1] == "O" || array [2][1] == "X") {
							System.out.println("Error: that position has already been selected");
							a = false;
							continue;
						}
						break;
					case 9: 
						if (array [2][2] == "O" || array [2][2] == "X") {
							System.out.println("Error: that position has already been selected");
							a = false;
							continue;
						}
						break;
					}
				 
				 break;
			 }
			 System.out.println("Error: you need to enter an integer");
			 myScanner.next();	
		 }
		 return position;
	}
	
	public static boolean checkWinner(String [][]array) {
		if (array[0][0] == array[0][1] && array[0][0] == array[0][2]) {
			System.out.println("Player " + array[0][0] + " has won");
			return true;
		}
		if (array[1][0] == array[1][1] && array[1][0] == array[1][2]) {
			System.out.println("Player " + array[1][0] + " has won");
			return true;
		}
		if (array[2][0] == array[2][1] && array[2][0] == array[2][2]) {
			System.out.println("Player " + array[2][0] + " has won");
			return true;
		}
		if (array[0][0] == array[1][0] && array[0][0] == array[2][0]) {
			System.out.println("Player " + array[0][0] + " has won");
			return true;
		}
		if (array[0][1] == array[1][1] && array[0][1] == array[2][1]) {
			System.out.println("Player " + array[0][1] + " has won");
			return true;
		}
		if (array[0][2] == array[1][2] && array[0][2] == array[2][2]) {
			System.out.println("Player " + array[0][2] + " has won");
			return true;
		}
		if (array[0][0] == array[1][1] && array[0][0] == array[2][2]) {
			System.out.println("Player " + array[0][0] + " has won");
			return true;
		}
		if (array[0][2] == array[1][1] && array[2][0] == array[0][2]) {
			System.out.println("Player " + array[0][2] + " has won");
			return true;
		}
		return false;
	}
	
	public static boolean checkDraw(String[][]array) {
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			for (int k = 0; k < array[i].length; k++) {
				if(array[i][k] == "X" || array[i][k] == "O") {
					count++;
				}
			}
		}
		if (count == 9) {
			System.out.println("The game is a draw");
			return true;
		}
		else {
			return false;
		}
	}
}
