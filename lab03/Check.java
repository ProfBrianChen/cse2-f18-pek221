/////////////////////
//// Pierre Klur
//// 9/7/18
//// CSE 002 - 110: Lab 03: Check
//// The pupose of this porgram is to write a program that
//// uses the Scanner class to obtain from the user the original 
//// cost of the check, the percentage tip they wish to pay, and
//// the number of ways the check will be split. Then determine 
//// how much each person in the group needs to spend in order to pay the check.

import java.util.Scanner; // imports the scanner

public class Check{
    			// main method required for every Java program
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in ); // declare the scanner
          System.out.print("Enter the original cost of the check in the form xx.xx: "); // ask the user for the cost of the check
          double checkCost = myScanner.nextDouble(); // get the user input
          System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):  "); // ask the user what percentage he/she wants to tip
          double tipPercent = myScanner.nextDouble(); // get user input 
          tipPercent /= 100; // convert the percentage into a decimal value
          System.out.print("Enter the number of people who went out to dinner: "); // ask the user how many people there were
          int numPeople = myScanner.nextInt(); // get the user input 
          
          // Declare all the variables that will be needed for the calculations:
          double totalCost;
          double costPerPerson;
          int dollars, dimes, pennies; 
         
          totalCost = checkCost * (1 + tipPercent); // calculate the total cost 
          costPerPerson = totalCost / numPeople; // calculate the cost per person 
          dollars=(int)costPerPerson; // get number of dollars as an integer

          dimes=(int)(costPerPerson * 10) % 10; // get number of dimes as an integer
          pennies=(int)(costPerPerson * 100) % 10; // get number of pennies as an integer
          System.out.println("Each person in the group owes: $" + dollars + "." + dimes + pennies ); // print the amount that each person has to pay in dollars, dimes and pennies

}  //end of main method   
  	} //end of class
