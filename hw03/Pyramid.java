/////////////////////
//// Pierre Klur
//// 9/10/18
//// CSE 002 - 110: HW 03: Pyramid
// The pupose of this porgram is to compute the volume inside 
// a pyramid with the dimensions given by the user

import java.util.Scanner; // imports the scanner

public class Pyramid{
       // main method required for every java program
       public static void main(String[] args) {
         
         
         Scanner myScanner = new Scanner( System.in ); // declares the scanner
         System.out.print("Enter the lenth of the square side of the pyramid: "); // asks the user for the length of the square side of the pyramid
         double length = myScanner.nextDouble(); // gets the length input from the user 
         System.out.print("Enter the height of the pyramid: "); // asks the user for the height of the pyramid 
         double height = myScanner.nextDouble(); // gets the height input from the user
         double volume; // declares the volume variable
         
         volume = length * length * height * (1.0/3.0); // caluclates the volume given the user inputs
         
         System.out.println("The volume inside the pyramid is: " + volume); // prints the volume inside the pyramid
      
       }
}