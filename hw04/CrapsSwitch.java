/////////////////////
//// Pierre Klur
//// 9/10/18
//// CSE 002 - 110: HW 04: Craps Switch
// The pupose of this porgram is to simulate the game of craps
// using a random number generator or asking the user for inputs. 
// This version uses switch statements rather than if statements.

import java.util.Scanner; // imports the scanner

public class CrapsSwitch{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in ); // declare the scanner
          System.out.println("Would you like randomly cast dice (enter 1) or would you like to state the two dice you want to evaluate (enter 2)?"); // asks the user if they want to user random dice values of choose them
          int userInput = myScanner.nextInt(); // get the user input
          
          
          switch (userInput){
            case 1:
            int rand1 = (int) (Math.random() * 6 + 1); // generates a random number [1,6] that will indicate the value of dice1
            int rand2 = (int) (Math.random() * 6 + 1); // generates a random number [1,6] that will indicate the value of dice2
            
            System.out.println(rand1);
            System.out.println(rand2);
            
              switch (rand1){
                case 1:
                  switch (rand2){
                    case 1:
                      System.out.println("Snake Eyes");
                      break;
                    case 2:
                      System.out.println("Ace Deuce");
                      break;
                    case 3:
                      System.out.println("Easy Four");
                      break;
                    case 4:
                      System.out.println("Fever Five");
                      break;
                    case 5:
                      System.out.println("Easy Six");
                      break;
                    case 6:
                      System.out.println("Seven Out");
                      break;
                  }
                   break;
                case 2:
                  switch (rand2){
                    case 1:
                      System.out.println("Ace Deuce");
                      break;
                    case 2:
                      System.out.println("Hard Four");
                      break;
                    case 3:
                      System.out.println("Fever Five");
                      break;
                    case 4:
                      System.out.println("Easy Six");
                      break;
                    case 5:
                      System.out.println("Seven Out");
                      break;
                    case 6:
                      System.out.println("Easy Eight");
                      break;
                  }
                  break; 
                case 3:
                  switch (rand2){
                    case 1:
                      System.out.println("Easy Four");
                      break;
                    case 2:
                      System.out.println("Fever Five");
                      break;
                    case 3:
                      System.out.println("Hard Six");
                      break;
                    case 4:
                      System.out.println("Seven Out");
                      break;
                    case 5:
                      System.out.println("Easy Eight");
                      break;
                    case 6:
                      System.out.println("Nine");
                      break;
                  }
                  break;
                case 4:
                  switch (rand2){
                    case 1:
                      System.out.println("Fever Five");
                       break;
                    case 2:
                      System.out.println("Easy Six");
                      break;
                    case 3:
                      System.out.println("Seven Out");
                      break;
                    case 4:
                      System.out.println("Hard Eight");
                      break;
                    case 5:
                      System.out.println("Nine");
                      break;
                    case 6:
                      System.out.println("Easy Ten");
                      break;
                  }
                  break;
                case 5:
                  switch (rand2){
                    case 1:
                      System.out.println("Easy Sis");
                      break;
                    case 2:
                      System.out.println("Seven Out");
                      break;
                    case 3:
                      System.out.println("Easy Eight");
                      break;
                    case 4:
                      System.out.println("Nine");
                      break;
                    case 5:
                      System.out.println("Hard Ten");
                      break;
                    case 6:
                      System.out.println("Yo-leven");
                      break;
                  }
                  break;
                case 6:
                  switch (rand2){
                    case 1:
                      System.out.println("Seven Out");
                      break;
                    case 2:
                      System.out.println("Easy Eight");
                      break;
                    case 3:
                      System.out.println("Nine");
                      break;
                    case 4:
                      System.out.println("Easy Ten");
                      break;
                     case 5:
                      System.out.println("Yo-leven");
                      break;
                    case 6:
                      System.out.println("Boxcars");
                      break;
                  }
                  break;
              }
              break;
            
            case 2:
              System.out.println("Enter the value of the two dice that you would like to use: "); // asks the user for the value of the two dice 
              int dice1 = myScanner.nextInt(); // gets user inputs
              int dice2 = myScanner.nextInt();
     
              switch (dice1){
                case 1:
                  switch (dice2){
                    case 1:
                      System.out.println("Snake Eyes");
                      break;
                    case 2:
                      System.out.println("Ace Deuce");
                      break;
                    case 3:
                      System.out.println("Easy Four");
                      break;
                    case 4:
                      System.out.println("Fever Five");
                      break;
                    case 5:
                      System.out.println("Easy Six");
                      break;
                    case 6:
                      System.out.println("Seven Out");
                      break;
                  }
                   break;
                case 2:
                  switch (dice2){
                    case 1:
                      System.out.println("Ace Deuce");
                      break;
                    case 2:
                      System.out.println("Hard Four");
                      break;
                    case 3:
                      System.out.println("Fever Five");
                      break;
                    case 4:
                      System.out.println("Easy Six");
                      break;
                    case 5:
                      System.out.println("Seven Out");
                      break;
                    case 6:
                      System.out.println("Easy Eight");
                      break;
                  }
                  break; 
                case 3:
                  switch (dice2){
                    case 1:
                      System.out.println("Easy Four");
                      break;
                    case 2:
                      System.out.println("Fever Five");
                      break;
                    case 3:
                      System.out.println("Hard Six");
                      break;
                    case 4:
                      System.out.println("Seven Out");
                      break;
                    case 5:
                      System.out.println("Easy Eight");
                      break;
                    case 6:
                      System.out.println("Nine");
                      break;
                  }
                  break;
                case 4:
                  switch (dice2){
                    case 1:
                      System.out.println("Fever Five");
                       break;
                    case 2:
                      System.out.println("Easy Six");
                      break;
                    case 3:
                      System.out.println("Seven Out");
                      break;
                    case 4:
                      System.out.println("Hard Eight");
                      break;
                    case 5:
                      System.out.println("Nine");
                      break;
                    case 6:
                      System.out.println("Easy Ten");
                      break;
                  }
                  break;
                case 5:
                  switch (dice2){
                    case 1:
                      System.out.println("Easy Sis");
                      break;
                    case 2:
                      System.out.println("Seven Out");
                      break;
                    case 3:
                      System.out.println("Easy Eight");
                      break;
                    case 4:
                      System.out.println("Nine");
                      break;
                    case 5:
                      System.out.println("Hard Ten");
                      break;
                    case 6:
                      System.out.println("Yo-leven");
                      break;
                  }
                  break;
                case 6:
                  switch (dice2){
                    case 1:
                      System.out.println("Seven Out");
                      break;
                    case 2:
                      System.out.println("Easy Eight");
                      break;
                    case 3:
                      System.out.println("Nine");
                      break;
                    case 4:
                      System.out.println("Easy Ten");
                      break;
                     case 5:
                      System.out.println("Yo-leven");
                      break;
                    case 6:
                      System.out.println("Boxcars");
                      break;
                  }
                  break;
              }
              break;

          }
        }
}