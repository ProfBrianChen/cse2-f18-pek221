/////////////////////
//// Pierre Klur
//// 11/26/18
//// CSE 002 - 110: hw09 - Linear
// The purpose of this hw is to practice with arrays
// and in searching single dimensional arrays

import java.util.Scanner; // import scanner
import java.util.Random; // import random

public class CSE2Linear {

	public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    boolean a = false;
    int[] grades = new int[15];

    System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
     
    // this is for the grades[0] because there was a runtime error when I chekced whether grades[0] was greater than or equal to the previous integer
    while (a == false){  
      a = myScanner.hasNextInt();
      if (a == true){
        grades[0] = myScanner.nextInt();
        break;      
      }
      System.out.println("Error: You need to enter an integer");
      myScanner.next();	          
      
      while (grades[0] < 0 || grades[0] > 100){
        System.out.println("Error: the value is outside of the specified range");   
        grades[0] = myScanner.nextInt(); 
      }
    }
    
    // This for-loop takes the user inputs for each grade after the first one and makes sure they meet the necessary conditions
    for (int i = 1; i < grades.length; i++){ 
      a = false;
      while (a == false){  
        a = myScanner.hasNextInt();
        if (a == true){
          grades[i] = myScanner.nextInt();    
        }
        else{
          System.out.println("Error: You need to enter an integer");
          myScanner.next(); 
        }	          
        while (grades[i] < 0 || grades[i] > 100){
          System.out.println("Error: The value is outside of the specified range");
          grades[i] = myScanner.nextInt(); 
        }
        while (grades[i] < grades[i-1]){
          System.out.println("Error: The integer must be greator than or equal to the last one");  
          grades[i] = myScanner.nextInt(); 
          }
      }  
    }
    
    // Prints the grades using the print method
    System.out.print("Grades: ");
    print(grades);
    System.out.println();
    
    // Searches for a specified grade using the binary search method and keeps track of the number of iterations
    System.out.println("Enter a grade to be searched for");
    int key = myScanner.nextInt();
    int length = grades.length - 1;
    int[] counter = new int[1];
    counter[0] = 0;
    int keyIndex = binarySearch(counter, grades, length, key);
    if (keyIndex == -1)   {System.out.println(key + " was not found with " + counter[0] + " iterations.");}   
    else {System.out.println("Found " + key + " at index " + keyIndex + " with " + counter[0] + " iterations.");}
 
    // Scrambles the grades array using the scramble method
    System.out.println("Srambled: ");
    scramble(grades);
    print(grades);
  
     // Searches for a specified grade using the linear search method and keeps track of the number of iterations
    System.out.println("Enter a grade to be searched for");
    key = myScanner.nextInt();
    length = grades.length - 1;
    counter[0] = 0;
    keyIndex = linearSearch(counter, grades, length, key);
    if (keyIndex == -1)   {System.out.println(key + " was not found with " + counter[0] + " iterations.");}   
    else {System.out.println("Found " + key + " at index " + keyIndex + " with " + counter[0] + " iterations.");}
    
    
  } // end of the main method
  
  
  
  // prints the array that is passed into it
  public static void print(int[] grades){
    for (int i = 0; i < grades.length; i++){
      System.out.print(grades[i] + " ");
    }  
    System.out.println();
  }
   
  // uses binary search to find a specified grade
  public static int binarySearch(int counter [], int grades [], int length, int key) {
     int mid;
     int low;
     int high;
     low = 0;
     high = length ;

     while (high >= low) {
       counter[0]++;
       mid = (high + low) / 2;  
       if (grades[mid] < key) {
         low = mid + 1;
       } 
       else if (grades[mid] > key) {
         high = mid - 1;
       } 
       else {
         return mid; 
       }
     }
      return -1; // not found
   }
  
  // uses linear search to find a specified grade
   public static int linearSearch(int counter [], int grades[], int length, int key) {
      for (int i = 0; i < length; ++i) {
        counter[0]++ ;
        if (grades[i] == key) {
            return i;
         }
      }
      return -1; /* not found */
   }
  
  // Scrambles the values in the array
  public static void scramble(int[] grades) {
	Random rand = new Random();
	int index;
	int zero;
	for (int i = 0; i < 100; i++) {
		index = rand.nextInt(grades.length-1) + 1;
		zero = grades[0];
		grades[0] = grades[index];
		grades[index] = zero;
	}
}
  
}// end of the class