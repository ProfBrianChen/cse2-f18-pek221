/////////////////////
//// Pierre Klur
//// 11/9/18
//// CSE 002 - 110: HW 08: Shuffling
// The purpose of this program is to practice in manipulating
// arrays and in writing methods that have array parameters.


import java.util.Scanner;
import java.util.Random;


public class Shuffling{ 
	
public static void main(String[] args) {
	
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  //System.out.print(cards[i]+" "); 
} 
//System.out.println();
printArray(cards); 
shuffle(cards);
System.out.println("Shuffled:");
printArray(cards);
System.out.println();
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   System.out.println("Hand:");
   printArray(hand);
   System.out.println();
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  
} 

//prints whatever array is sent to this method
public static void printArray(String[] list) {
	for (int i = 0; i < list.length; i++) {
		System.out.print(list[i] + " ");
	}
	System.out.println();
}

// shuffles the cards in the array that is sent to this method
public static void shuffle(String[] list) {
	Random rand = new Random();
	int index;
	String zero;
	for (int i = 0; i < 100; i++) {
		index = rand.nextInt(51) + 1;
		zero = list[0];
		list[0] = list[index];
		list[index] = zero;
	}
	System.out.println();
}

// Gets a hand from the end of the array that is composed of numCards cards.
public static String[] getHand(String[] list, int index, int numCards) {
	String[] hand = {"","","","",""};
	for (int j = 0; j < numCards; j++) {
		hand[j] = list[index - j];
	}
	return hand;
}

}



