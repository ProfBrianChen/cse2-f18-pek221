/////////////////////
//// Pierre Klur
//// 10/7/18
//// CSE 002 - 110: HW 06: Encrypted X
// The pupose of this porgram is to use loops to create a big X
// by having spaces placed strategically in a grid of stars

import java.util.Scanner; // imports the scanner

public class EncryptedX {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); // declares the scanner

    // declare and initialize the variables
		int i = 1;
		int j = 1;
		int input = 0;
		boolean a = false;

		// This while-loop gets the user input for the number of hands and makes sure it is an integer
		while (a == false){
			//ask user for the number of hands
			System.out.println("Enter an integer between 1 and 100. ");
			//use scanner to accept the input
			a = myScanner.hasNextInt();
			// if the input is the right type, this if statement assigns the value given to the variable
			if (a == true){
				input = myScanner.nextInt();
				System.out.println("");
				break;
			}
			System.out.println("You need to enter an integer");
			//clear Scanner 
			myScanner.next();
		}
		
		
		for (i = 1; i <= input; i++) { // the outer for-loop relates to the each row that is generated, based on the input value
			
	        for (j = 1; j <= input; j++){ // the inner for-loop relates to the columns and makes sure there are enough stars and spaces in the right spots
	        	if ((j == i) || (j == (input + 1 - i)) ) 
	        	{System.out.print(" ");}
	        	else 
	        	{System.out.print("*");}
	        }
	        System.out.println("");	
		}
			
	}	
}