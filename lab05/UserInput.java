/////////////////////
//// Pierre Klur
//// 10/7/18
//// CSE 002 - 110: Lab 05: User Input
//// The pupose of this porgram is to write 
//// loops that asks the user to enter information
//// relating to a course they are currently taking.

import java.util.Scanner; // imports the scanner

public class UserInput{
    			// main method required for every Java program
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in ); // declare the scanner
          
          //declare booleans to check whether the input is the right type
          boolean a = false;
          boolean b = false;
          boolean c = false;
          boolean d = false;
          boolean e = false;
          boolean f = false;
  
          //declare the integers and strings that correspond to each required input
          int courseNum = 0;
          String depName = "";
          int numTimes = 0;
          String startTime = "";
          String instructorName = "";
          int numStudents = 0;
  
          //while loop for the course number
          while (a == false){
            //ask user for the course number
            System.out.println("Input the course number: ");
            //use scanner to accept the input
            a = myScanner.hasNextInt();
            // if the input is the right type, this if statement assigns the value given to the variable
            if (a == true){
              courseNum = myScanner.nextInt();
              break;
            }
            System.out.println("You need to enter an integer");
            //clear Scanner 
            myScanner.next();
          }
         
          
          //while loop for the Department Name
          while (b == false){
            //ask user for the department name
            System.out.println("Input the department name: ");
            //use scanner to accept the input
            b = myScanner.hasNext();
            // if the input is the right type, this if statement assigns the value given to the variable
            if (b == true){
              depName = myScanner.next();
              break;
            }
            System.out.println("You need to enter an string");
            //clear Scanner 
            myScanner.next();
          }
          
          
          //while loop for the number of times the class meets
          while (c == false){
            //ask user how many times the class meets
            System.out.println("Input how many times the class meets: ");
            //use scanner to accept the input
            c = myScanner.hasNextInt();
            // if the input is the right type, this if statement assigns the value given to the variable
            if (c == true){
              numTimes = myScanner.nextInt();
              break;
            }
            System.out.println("You need to enter an integer");
            //clear Scanner 
            myScanner.next();
          }
          
          
          
           //while loop for the start time
          while (d == false){
            //ask user for the start time
            System.out.println("Input the start time: ");
            //use scanner to accept the input
            d = myScanner.hasNext();
            // if the input is the right type, this if statement assigns the value given to the variable
            if (d == true){
              startTime = myScanner.next();
              break;
            }
            System.out.println("You need to enter a string");
            //clear Scanner 
            myScanner.next();
          }
          
          
           //while loop for the instructor's name
          while (e == false){
            //ask user for the instructor's name
            System.out.println("Input the intrcutor's name: ");
            //use scanner to accept the input
            e = myScanner.hasNext();
            // if the input is the right type, this if statement assigns the value given to the variable
            if (e == true){
              instructorName = myScanner.next();
              break;
            }
            System.out.println("You need to enter a string");
            //clear Scanner 
            myScanner.next();
          }
          
          
          
           //while loop for the number of students in the class
          while (f == false){
            //ask user for the number of students
            System.out.println("Input the number of students in the class: ");
            //use scanner to accept the input
            f = myScanner.hasNextInt();
            // if the input is the right type, this if statement assigns the value given to the variable
            if (f == true){
              numStudents = myScanner.nextInt();
              break;
            }
            System.out.println("You need to enter an integer");
            //clear Scanner 
            myScanner.next();
          }
          
          
          // Prints a summary of the course information
          System.out.println(" ");
          System.out.println("The course number is: " + courseNum);
          System.out.println("The department name is: " + depName);
          System.out.println("The class meets " + numTimes + " times per week");
          System.out.println("The class starts at: " + startTime);
          System.out.println("The instructor name is: " + instructorName);
          System.out.println("There are " + numStudents + " students in the class");
          
        }
}