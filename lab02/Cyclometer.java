/////////////////////
//// Pierre Klur
//// 9/7/18
//// CSE 002 - 110: Lab 02: Cyclometer
//// The pupose of this porgram is to record time and rotation count. And:
// print the number of minutes for each trip
// print the number of counts for each trip
// print the distance of each trip in miles
// print the distance for the two trips combined 

public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      int secsTrip1=480;  // declare the variable for number of seconds in trip 1
      int secsTrip2=3220;  // declare the variable for number of seconds in trip 2
		  int countsTrip1=1561;  // declare the variable for number roations in trip 1
		  int countsTrip2=9037; // declare the variable for number roations in trip 2

      double wheelDiameter=27.0,  // declare the double variable for the wheel diameter
  	  PI=3.14159, // declare the value of PI
  	  feetPerMile=5280,  // declare the number of feet per mile 
  	  inchesPerFoot=12,   // decalre the number of inches per foot
  	  secondsPerMinute=60;  // declare the number of seconds per minute
	    double distanceTrip1, distanceTrip2,totalDistance;  // declare the double variables for the distance of each trip and the total distance 
      
      // Calculate the number of minutes and rotations for the first trip and print the results
      System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts."); 
      // Calculate the number of minutes and rotations for the second trip and print the results
	    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
      
      distanceTrip1=countsTrip1*wheelDiameter*PI; //calculate the distance of trip 1 in inches 
      // For each count, a rotation of the wheel travels the diameter in inches times PI
	    distanceTrip1/=inchesPerFoot*feetPerMile; // calculate the distance of trip 1 in miles
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // calculate the distance of trip 2 in miles
	    totalDistance=distanceTrip1+distanceTrip2; // calculate the total distance of both trips 
      
      
       System.out.println("Trip 1 was "+distanceTrip1+" miles"); // Print the distance of trip 1 
	     System.out.println("Trip 2 was "+distanceTrip2+" miles"); // Print the distacne of trip 2 
       System.out.println("The total distance was "+totalDistance+" miles"); // Print the distance of both trips combined


	}  //end of main method   
} //end of class
