/////////////////////
//// Pierre Klur
//// 9/10/18
//// CSE 002 - 110: HW 02: Arithmetic Calculations
// The pupose of this porgram is to compute the cost of the
// items purchased from a store, including a 6% sales tax. 

public class ArithmeticCalculations{
  
  public static void main(String args[]){
    // Below are the assumtions for the problem that are given
    int numPants = 3; //Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants
    int numShirts = 2; //Number of sweatshirts
    double shirtPrice = 24.99; //Cost per shirt
    int numBelts = 1; //Number of belts
    double beltCost = 33.99; //cost per belt     
    double paSalesTax = 0.06; //the tax rate
    
    double totalCostOfPants; // declares the variable for the total cost of pants
    double totalCostOfShirts; // declares the variable for the total cost of shirts
    double totalCostOfBelts; // declares the variable for the totale number of belts
    
    double salesTaxOnPants; // declares the variable for the sales tax on pants
    double salesTaxOnShirts; // declares the variable for the sales tax on shirts 
    double salesTaxOnBelts; // declares the variable for the sales tax on belts 
    
    double totalCostOfPurchase; // declares the variable for the total cost of the purchase before tax 
    double totalSalesTax; // declares the variable for the total sales tax 
    double totalPaid; // declares the variable for the total amount paid after the sales tax 
    
    totalCostOfPants = numPants * pantsPrice; // Calculates the total cost of pants
    totalCostOfShirts = numShirts * shirtPrice; // Calculates the total cost of shirts 
    totalCostOfBelts = numBelts * beltCost; // Calculates the total cost of belts 
    
    salesTaxOnPants = totalCostOfPants * paSalesTax; // Calculates the sales tax on pants 
    salesTaxOnShirts = totalCostOfShirts * paSalesTax; // Calculates the sales tax on shirts
    salesTaxOnBelts = totalCostOfBelts * paSalesTax; // Calculates the sales tax on belts 
    
    salesTaxOnPants = salesTaxOnPants *100; // This line and the following line convert the value of the sales tax to a number with only two decimal places
    salesTaxOnPants = (int) salesTaxOnPants / 100.0;
    
    salesTaxOnShirts = salesTaxOnShirts *100; // This line and the following line convert the value of the sales tax to a number with only two decimal places
    salesTaxOnShirts = (int) salesTaxOnShirts / 100.0;
    
    salesTaxOnBelts = salesTaxOnBelts *100; // This line and the following line convert the value of the sales tax to a number with only two decimal places
    salesTaxOnBelts = (int) salesTaxOnBelts / 100.0;
    
    
    totalCostOfPurchase = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; // Calculates the total price of the purchase before tax
    totalSalesTax = salesTaxOnPants + salesTaxOnShirts + salesTaxOnBelts; // Calculates the total sales tax of the purchase
    totalPaid = totalCostOfPurchase + totalSalesTax; // Calculates the total amount paid after the sales tax 
    
    System.out.println(" "); // I used this print statement to make the output easier to read in the terminal
    System.out.println("The total cost of pants is: $" + totalCostOfPants + " and the total sales tax on pants is: $" + salesTaxOnPants); // prints the total cost and sales tax for the pants
    System.out.println("The total cost of shirts is: $" + totalCostOfShirts + " and the total sales tax on shirts is: $" + salesTaxOnShirts); // prints the total cost and sales tax for the shirts
    System.out.println("The total cost of belts is: $" + totalCostOfBelts + " and the total sales tax on belts is: $" + salesTaxOnBelts); // prints the total cost and sales tax of the belts
    System.out.println(" "); // makes a gap between the output to make it easier to read
    System.out.println("The total cost of purchases (before tax) is: $" + totalCostOfPurchase); // prints the total cost of the purchase before the tax
    System.out.println("The total sales tax is: $" + totalSalesTax); // prints the total sales tax
    System.out.println("The total amount paid is: $" + totalPaid); // prints the total amount paid 
    
  }
}


