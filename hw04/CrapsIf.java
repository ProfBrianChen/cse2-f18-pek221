/////////////////////
//// Pierre Klur
//// 9/10/18
//// CSE 002 - 110: HW 03: Craps If
// The pupose of this porgram is to simulate the game of craps 
// using a random number generator or asking the user for inputs. 
// This version uses if statements rather than switch statements.

import java.util.Scanner; // imports the scanner

public class CrapsIf{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in ); // declare the scanner
          System.out.println("Would you like randomly cast dice (enter 1) or would you like to state the two dice you want to evaluate (enter 2)?"); // asks the user if they want to user random dice values of choose them
          int userInput = myScanner.nextInt(); // get the user input
          
          
          if (userInput == 1){
            int rand1 = (int) (Math.random() * 6 + 1); // generates a random number [1,6] that will indicate the value of dice1
            int rand2 = (int) (Math.random() * 6 + 1); // generates a random number [1,6] that will indicate the value of dice2
            
            System.out.println(rand1);
            System.out.println(rand2);
            
            // The sequence of if statements that follow make up all the possible combinations of craps hands and prints
            // the proper slang name for each one.
            if (rand1 + rand2 == 2){
              System.out.println("Snake Eyes");
            }
            if ((rand1 + rand2 == 3)){
              System.out.println("Ace Deuce");
            }
            if (((rand1 == 1) && (rand2 == 3)) || ((rand1 == 3) && (rand2 == 1))){
              System.out.println("Easy Four");
            }
            if (((rand1 == 1) && (rand2 == 4)) || ((rand1 == 4) && (rand2 == 1))){
              System.out.println("Fever Five");
            }
            if (((rand1 == 1) && (rand2 == 5)) || ((rand1 == 5) && (rand2 == 1))){
              System.out.println("Easy Six");
            }
            if (((rand1 == 1) && (rand2 == 6)) || ((rand1 == 6) && (rand2 == 1))){
              System.out.println("Seven Out");
            }
            if ((rand1 == 2) && (rand2 == 2)){
              System.out.println("Hard Four");
            }
            if (((rand1 == 2) && (rand2 == 3)) || ((rand1 == 3) && (rand2 == 2))){
              System.out.println("Fever Five");
            }
            if (((rand1 == 2) && (rand2 == 4)) || ((rand1 == 4) && (rand2 == 2))){
              System.out.println("Easy Six");
            }
            if (((rand1 == 5) && (rand2 == 2)) || ((rand1 == 2) && (rand2 == 5))){
              System.out.println("Seven Out");
            }
            if (((rand1 == 2) && (rand2 == 6)) || ((rand1 == 6) && (rand2 == 2))){
              System.out.println("Easy Eight");
            }
            if ((rand1 == 3) && (rand2 == 3)){
              System.out.println("Hard Six");
            }
            if (((rand1 == 4) && (rand2 == 3)) || ((rand1 == 3) && (rand2 == 4))){
              System.out.println("Seven Out");
            }
            if (((rand1 == 5) && (rand2 == 3)) || ((rand1 == 3) && (rand2 == 5))){
              System.out.println("Easy Eight");
            }
            if (((rand1 == 6) && (rand2 == 3)) || ((rand1 == 3) && (rand2 == 6))){
              System.out.println("Nine");
            }
            if ((rand1 == 4) && (rand2 == 4)){
              System.out.println("Hard Eight");
            }
            if (((rand1 == 5) && (rand2 == 4)) || ((rand1 == 4) && (rand2 == 5))){
              System.out.println("Nine");
            }
            if (((rand1 == 6) && (rand2 == 4)) || ((rand1 == 4) && (rand2 == 6))){
              System.out.println("Easy Ten");
            }
            if ((rand1 == 5) && (rand2 == 5)){
              System.out.println("Hard Ten");
            }
            if (rand1 + rand2 == 11){
              System.out.println("Yo-leven");
            }
            if (rand1 + rand2 == 12){
              System.out.println("Boxcars");
            }
          }
          
          
          if (userInput == 2){
            System.out.println("Enter the value of the two dice that you would like to use: "); // asks the user for the value of the two dice 
            int dice1 = myScanner.nextInt(); // gets user inputs
            int dice2 = myScanner.nextInt();
                // This nested if statement makes sure that the user is choosing dice values within the 1-6 range
                if ((dice1 < 1 || (dice1 > 6)) || ((dice2 < 1) || (dice2 > 6))){
                  System.out.println("Error: impossible dice values");
                }
  
            // The sequence of if statements that follow make up all the possible combinations of craps hands and prints
            // the proper slang name for each one.
            if (dice1 + dice2 == 2){
              System.out.println("Snake Eyes");
            }
            if ((dice1 + dice2 == 3)){
              System.out.println("Ace Deuce");
            }
            if (((dice1 == 1) && (dice2 == 3)) || ((dice1 == 3) && (dice2 == 1))){
              System.out.println("Easy Four");
            }
            if (((dice1 == 1) && (dice2 == 4)) || ((dice1 == 4) && (dice2 == 1))){
              System.out.println("Fever Five");
            }
            if (((dice1 == 1) && (dice2 == 5)) || ((dice1 == 5) && (dice2 == 1))){
              System.out.println("Easy Six");
            }
            if (((dice1 == 1) && (dice2 == 6)) || ((dice1 == 6) && (dice2 == 1))){
              System.out.println("Seven Out");
            }
            if ((dice1 == 2) && (dice2 == 2)){
              System.out.println("Hard Four");
            }
            if (((dice1 == 2) && (dice2 == 3)) || ((dice1 == 3) && (dice2 == 2))){
              System.out.println("Fever Five");
            }
            if (((dice1 == 2) && (dice2 == 4)) || ((dice1 == 4) && (dice2 == 2))){
              System.out.println("Easy Six");
            }
            if (((dice1 == 5) && (dice2 == 2)) || ((dice1 == 2) && (dice2 == 5))){
              System.out.println("Seven Out");
            }
            if (((dice1 == 2) && (dice2 == 6)) || ((dice1 == 6) && (dice2 == 2))){
              System.out.println("Easy Eight");
            }
            if ((dice1 == 3) && (dice2 == 3)){
              System.out.println("Hard Six");
            }
            if (((dice1 == 4) && (dice2 == 3)) || ((dice1 == 3) && (dice2 == 4))){
              System.out.println("Seven Out");
            }
            if (((dice1 == 5) && (dice2 == 3)) || ((dice1 == 3) && (dice2 == 5))){
              System.out.println("Easy Eight");
            }
            if (((dice1 == 6) && (dice2 == 3)) || ((dice1 == 3) && (dice2 == 6))){
              System.out.println("Nine");
            }
            if ((dice1 == 4) && (dice2 == 4)){
              System.out.println("Hard Eight");
            }
            if (((dice1 == 5) && (dice2 == 4)) || ((dice1 == 4) && (dice2 == 5))){
              System.out.println("Nine");
            }
            if (((dice1 == 6) && (dice2 == 4)) || ((dice1 == 4) && (dice2 == 6))){
              System.out.println("Easy Ten");
            }
            if ((dice1 == 5) && (dice2 == 5)){
              System.out.println("Hard Ten");
            }
            if (dice1 + dice2 == 11){
              System.out.println("Yo-leven");
            }
            if (dice1 + dice2 == 12){
              System.out.println("Boxcars");
            }
          }        
      }
    }
                
                