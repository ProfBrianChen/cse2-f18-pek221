/////////////////////
//// Pierre Klur
//// 10/12/18
//// CSE 002 - 110: Lab 06: PatternC
// The purpose of this program is to use nested loops to
// print various outputs

import java.util.Scanner; // imports the scanner

public class PatternC {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); // declares the scanner
		
    int k = 0;
    int i = 0;
		int input = 0;
		int numRows = 1;
		boolean a = false;
		
		// This while-loop gets the user input for the number of hands and makes sure it is an integer
			while (a == false){
			    //ask user for the number of hands
				System.out.println("Enter an integer between 1 and 10. ");
			    //use scanner to accept the input
			    a = myScanner.hasNextInt();
			    // if the input is the right type, this if statement assigns the value given to the variable
			         if (a == true){
			            input = myScanner.nextInt();
			            break;
			         }
			    System.out.println("You need to enter an integer");
			     //clear Scanner 
			    myScanner.next();
			 }
    k = input;
    
			for (numRows = 1; numRows <= input; numRows++) {
				for (k = 0; k <= numRows; k++) {
            System.out.print(" ");
        }
        for (i = numRows; i >= 1; i--) {
          System.out.print(i);
        } 
        System.out.println(" ");
			}
			
	}

}