/////////////////////
//// Pierre Klur
//// 9/21/18
//// CSE 002 - 110: Lab 04: Card Generator
//// The pupose of this porgram is to use a random generator to help 
//// a magician practice his card tricks by randomly picking a card with
//// suit and value. 

public class CardGenerator{
    			// main method required for every Java program
   			public static void main(String[] args) {
          
          int cardRandom = (int) (Math.random() * 13 + 1); // generates a random number [1,13] that wil indicate the identity of the card (either the number or name of the face card)
          int suitRandom = (int) (Math.random() * 4 + 1); // generates a random number [1,4] that will indicate the suit
          String suit; // declares the string for the suit
          String identity; // declares the string for the identity of the card 
          

          //////////
          ////////// Diamonds
          //////////
          
          if (suitRandom == 1){
            suit = "Diamonds";
            /* This if statement determines if the suit is diamonds and if so, there are a sequence of if statements nested inside that
            will print the output statement based on what the identity of the card is. The ace and face cards have their own nested if statements
            because the identity has to be changed from a number to a word.
            */
            if (cardRandom > 1 && cardRandom < 11){
              System.out.println(" You picked the " + cardRandom + " of " + suit); // prints output for non ace/face cards
            }
            else if (cardRandom == 1){
              identity = "Ace";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the ace
            }
            else if (cardRandom == 11){
              identity = "Jack";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the jack
            }
            else if (cardRandom == 12){
              identity = "Queen";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the Queen
            }
            else if (cardRandom == 13){
              identity = "King";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the King
            }
          }
          
          
          
           //////////
          ////////// Clubs
          //////////
          
          if (suitRandom == 2){
            suit = "Clubs";
            /* This if statement determines if the suit is clubs and if so, there are a sequence of if statements nested inside that
            will print the output statement based on what the identity of the card is. The ace and face cards have their own nested if statements
            because the identity has to be changed from a number to a word.
            */
            if (cardRandom > 1 && cardRandom < 11){
              System.out.println(" You picked the " + cardRandom + " of " + suit); // prints output for non ace/face cards
            }
            else if (cardRandom == 1){
              identity = "Ace";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the Ace
            }
            else if (cardRandom == 11){
              identity = "Jack";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the Jack
            }
            else if (cardRandom == 12){
              identity = "Queen";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the Queen
            }
            else if (cardRandom == 13){
              identity = "King";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the King
            }
          }
          
          
           //////////
          ////////// Hearts
          //////////
          
          if (suitRandom == 3){
            suit = "Hearts";
            /* This if statement determines if the suit is hearts and if so, there are a sequence of if statements nested inside that
            will print the output statement based on what the identity of the card is. The ace and face cards have their own nested if statements
            because the identity has to be changed from a number to a word.
            */
            if (cardRandom > 1 && cardRandom < 11){
              System.out.println(" You picked the " + cardRandom + " of " + suit); // prints output for non ace/face cards
            }
            else if (cardRandom == 1){
              identity = "Ace";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the Ace
            }
            else if (cardRandom == 11){
              identity = "Jack";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the Jack
            }
            else if (cardRandom == 12){
              identity = "Queen";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the Queen
            }
            else if (cardRandom == 13){
              identity = "King";
              System.out.println("You picked the " + identity + " of " + suit);  // prints output for the King
            }
          }
          
          //////////
          ////////// Spades
          //////////
          
          if (suitRandom == 4){
            suit = "Spades";
            /* This if statement determines if the suit is spades and if so, there are a sequence of if statements nested inside that
            will print the output statement based on what the identity of the card is. The ace and face cards have their own nested if statements
            because the identity has to be changed from a number to a word.
            */
            if (cardRandom > 1 && cardRandom < 11){
              System.out.println(" You picked the " + cardRandom + " of " + suit); // prints output for non ace/face cards
            }
            else if (cardRandom == 1){
              identity = "Ace";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the Ace
            }
            else if (cardRandom == 11){
              identity = "Jack";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the Jack
            }
            else if (cardRandom == 12){
              identity = "Queen";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the Queen
            }
            else if (cardRandom == 13){
              identity = "King";
              System.out.println("You picked the " + identity + " of " + suit); // prints output for the King
            }
          }
        
          
        }
}