/////////////////////
//// Pierre Klur
//// 9/3/18
//// CSE 002 Welcome Class
//// HW 01
public class WelcomeClass{
  
  public static void main(String args[]){
    
    System.out.println("  -----------"); // Print the top line of the box
    System.out.println("  | WELCOME |"); // Print WELCOME
    System.out.println("  -----------"); // Print the bottom line of the box
    System.out.println(" ^  ^  ^  ^  ^  ^"); // Print the arrowheads on top 
    System.out.println(" /\\ /\\ /\\ /\\ /\\ /\\"); // Print the top dashes; had to use \\ because it was reading \ as an error 
    System.out.println("<-P--E--K--2--2--1->"); // Print my lehigh username pek221
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\/"); // Print the botto dashes; had to use \\ once again because of the error
    System.out.println("  v  v  v  v  v  v"); // Print the bottom arrowheads
  }
  
}