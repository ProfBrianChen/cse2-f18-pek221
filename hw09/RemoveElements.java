/////////////////////
//// Pierre Klur
//// 11/26/18
//// CSE 002 - 110: hw09 - Remove Elements
// The purpose of this hw is to practice with arrays
// and removing certain elements

import java.util.Scanner;
import java.util.Random;

public class RemoveElements{
	
  public static void main(String [] arg){
	  
	  Scanner scan=new Scanner(System.in);
	  int num[]=new int[10];
	  int newArray1[];
	  int newArray2[];
	  int index,target;
	  String answer="";
	  do{
		  System.out.print("Random input 10 ints [0-9]");
		  num = randomInput();
		  String out = "The original array is:";
		  out += listArray(num);
		  System.out.println(out);
 
		  System.out.print("Enter the index ");
		  index = scan.nextInt();
		  if(index >= num.length) {
			  System.out.println("The index is out of range");
		  }
		  else {
			  newArray1 = delete(num,index);
			  String out1="The output array is ";
			  out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
			  System.out.println(out1); 	
		  }
 
		  System.out.print("Enter the target value ");
		  target = scan.nextInt();
		  int elementCount = 0;
		  for(int i = 0; i < num.length; i++) {
			  if (num[i] == target) {
				  elementCount++;
			  }
		  }
		  if (elementCount == 0) {
			  System.out.println("The element " + target + " was not found");
		  }
		  else {
			  newArray2 = remove(num,target);
			  String out2="The output array is ";
			  out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
			  System.out.println(out2);
		  }
  	
		  System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
		  answer=scan.next();
	  }while(answer.equals("Y") || answer.equals("y"));
  }
 
 public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }

 public static int[] randomInput() {
	  int array[] = new int[10];
	  Random rand = new Random();
	  for(int i = 0; i < array.length; i++) {
		  array[i] = rand.nextInt(9);
	  }
	  return array;
  }

 public static int[] delete(int[] list, int pos) {
	  int array[] = new int[list.length - 1];
	  int index = 0;
	  for(int i = 0; i < array.length; i++) {
		  if (i == pos) {
			  index++;
		  }
		  array[i] = list[index];
		  index++;
	  }
	  return array;
  }

 public static int[] remove(int[] list, int target){
	  int counter = 0;
	  for (int i = 0; i < list.length; i++)
		  if (list[i] == target) {
			  counter++;
		  }
	  int array[] = new int[list.length - counter];
	  int index = 0;
	  for(int k = 0; k < array.length; k++) {
		  if (list[index] == target) {
			  index++;
		  }
		  array[k] = list[index];
		  index++;
	  }
	  return array;
  }
  

}
