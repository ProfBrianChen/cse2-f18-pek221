/////////////////////
//// Pierre Klur
//// 10/26/18
//// CSE 002 - 110: Lab 07: Methods
// The purpose of this program is to use methods 
// to generate artificial sentences/stories 

import java.util.Scanner; // imports the scanner
import java.util.Random;

public class Methods {

	public static void main(String[] args) { // main method of the program
		Scanner myScanner = new Scanner(System.in); // declares the scanner
		// declare the variables needed
		int i = 0;
		int j = 0;
		int input = 1;
   	 	Random randomGenerator = new Random(); // generate a random number which will correspond to a word
   	 	int randomInt = randomGenerator.nextInt(10);
    
		// Phase 1
		for (i=0; i <= 10; i++){ // loop that generates a sentence and then asks the user if they would like another one
			System.out.println("The" + Adjectives() + Subjects() + Verbs() + " the" + Adjectives() + Objects());  
			System.out.println("");
			System.out.println("Would you like another sentence? (Enter 1 for Yes and 2 for No)");
			input = myScanner.nextInt();
			if (input == 1) 
			{continue;}
			else 
			{break;}
		}
		System.out.println(Thesis());// prints the paragraph
		for (j=1; j<= randomInt; j++) {
			System.out.println(ActionSentence());
		}
	}

 
	/* Phase 0 : each of these methods generates a random number between 0 and 9 
	 * and associates a word to each potential digit. Then, it returns whatever 
	 * word corresponds to the digit generated */
	public static String Adjectives() {
		Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    
    switch (randomInt){
      case 0:
        return " red";
      case 1:
        return " blue";
      case 2:
        return " green";
      case 3:
        return " yellow";
      case 4:
        return " white";
      case 5: 
        return " black";
      case 6: 
        return " purple";
      case 7:
        return " orange";
      case 8:
        return " grey";
      case 9: 
        return " pink";
      default:
        return " error";
    }    
  }  
  
    public static String Subjects() {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
      
          switch (randomInt){
      case 0:
        return " monkey";
      case 1:
        return " person";
      case 2:
        return " dog";
      case 3:
        return " cat";
      case 4:
        return " cow";
      case 5: 
        return " zebra";
      case 6: 
        return " horse";
      case 7:
        return " tiger";
      case 8:
        return " lion";
      case 9: 
        return " elephant";
      default:
        return " error";
    }
  }  
  
    public static String Verbs() {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
      
          switch (randomInt){
      case 0:
        return " ate";
      case 1:
        return " threw";
      case 2:
        return " attacked";
      case 3:
        return " devoured";
      case 4:
        return " ingested";
      case 5: 
        return " nibbled";
      case 6: 
        return " crushed";
      case 7:
        return " bit";
      case 8:
        return " loved";
      case 9:
        return " hated";
      default:
        return " error";
    }
  }  
  
    public static String Objects() {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
      
          switch (randomInt){
      case 0:
        return " apple.";
      case 1:
        return " banana.";
      case 2:
        return " pear.";
      case 3:
        return " orange.";
      case 4:
        return " peach.";
      case 5: 
        return " watermelon.";
      case 6: 
        return " grape.";
      case 7:
        return " pineapple.";
      case 8:
        return " clementine.";
      case 9: 
        return " mango.";
      default:
        return " error";
    }
  }  
  
    // Phase 2
    public static String Thesis() {
    	return  "The" + Adjectives() + Subjects() + Verbs() + " the" + Adjectives() + Objects() ;
    }
    
    public static String ActionSentence() {
    	return "This" + Subjects() + " was" + Adjectives() + " and it" + Verbs() + " many more.";
    }
}


