/////////////////////
//// Pierre Klur
//// 9/10/18
//// CSE 002 - 110: HW 03: Convert
// The pupose of this porgram is to ask the user for values for the
// amount of rain from a hurricane and the number of acres affected. 
// The program will then use the user inputs to calculate the cubic mies of rain. 

import java.util.Scanner; // imports the scanner

public class Convert{
       // main method required for every Java program
       public static void main(String[] args) {
         
         Scanner myScanner = new Scanner( System.in ); // declare the scanner
         System.out.print("Enter the number of acres affected by the hurricane: "); // ask the user for the number of acres 
         double acres = myScanner.nextDouble(); // get the user input
         System.out.print("Enter the average rainfall in inches over the area affected: "); // ask the user for the amount of rainfall
         double rainfall = myScanner.nextDouble(); // get the user input
         double gallons; // declare the variable for gallons of rain
         double miles; // declare the varibale for the cubic miles of rain 
           
         gallons = rainfall * acres * 27154; // calculate the number of gallons by using the values of inches and acres as well as a constant conversion ratio
         miles = gallons * 0.000000000000908169; // calculate the cubic miles by using a conversion ratio found online.
         
         System.out.println("There were " + miles + " cubic miles of rain"); // print the number of cubic miles 
         
       }
}