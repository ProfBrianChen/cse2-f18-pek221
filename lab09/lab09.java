/////////////////////
//// Pierre Klur
//// 11/9/18
//// CSE 002 - 110: Lab 09: Arrays and Methods
// The purpose of this program is to get familiar with 
// passing arrays as method arguments


public class lab09 {

	public static void main(String[] args) {

		int[] array0 = new int[10];
		int[] array1 = new int[10];
		int[] array2 = new int[10];
		int[] array3 = new int[10];
		for (int i = 0; i < array0.length; i++) {
			array0[i] = i;
		}
		array1 = copy(array0);
		array2 = copy(array0);
		
		inverter(array0);
		System.out.print("array0: ");
		print(array0);
		System.out.println();
		inverter2(array1);
		System.out.print("array1: ");
		print(array1);
		System.out.println();
		array3 = inverter2(array2);
		System.out.print("array3: ");
		print(array3);		
    System.out.println();
	}

	public static int[] copy(int[] arrayIn) {
		int[] arrayOut = new int[arrayIn.length];
		for(int i = 0; i < arrayOut.length; i++) {
			arrayOut[i] = arrayIn[i];
		}
		return arrayOut;
	}
	
	public static void inverter(int[] array) {
		int val = 0;
		for(int i = 0; i < array.length/2; i++) {
			val = array[i];
			int index = array.length - i - 1;
			array[i] = array[index];
			array[index] = val;
		}
	}
	
	public static int[] inverter2(int[] array) {
		int[] copy = new int[array.length];
		copy = copy(array);
		inverter(copy);
		return copy;
	}
	
	public static void print(int[] array) {
		for(int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
	}
}
