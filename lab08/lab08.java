/////////////////////
//// Pierre Klur
//// 11/9/18
//// CSE 002 - 110: Lab 08: Arrays
// The purpose of this program is to get familiar with one-dimensional arrays.
// and write a program than randomly places integers between 0-99 into an array
// and then counts the number of occurrences of each integer.

import java.util.Arrays;
import java.util.Random;

public class lab08 {

	public static void main(String[] args) {
		int [] array1 = new int[100];
		int [] array2 = new int[100];		
    int [] array3 = new int[100];
    Random rand = new Random();
    
    for (int i = 0; i < array1.length; i++) {
			array1[i] = rand.nextInt(99);    
      for (int j = 0; j < array2.length; j++){
        if (array1[i] == j){
          array2[j]++;
        }
      }
		}

    for (int k = 0; k < array2.length; k++){
      System.out.println(k + " occurs " + array2[k] + " time(s)");
    }
    
	}
}
