/////////////////////
//// Pierre Klur
//// 10/26/18
//// CSE 002 - 110: HW 07: Word Tools
// The purpose of this program is to use methods 
// to analyze a text that is written by the user


import java.util.Scanner; // imports the scanner

public class hw07 {
	
  public static void main(String[] args){ // Main method used in all java files

		//declare and initialize variables
		int a = 0;
		String input = ""; 
		String sampleText = sampleText(); //get text input from user
		while(a < 1){ 
			switch(input){ // switch statement accesses a different method based on the user input
			// for each case, we call a method that gives you the output required
				case "c":
					System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(sampleText)); 
					input = "";  // resets the input for the next time around the loop
					break;
				case "w":
					System.out.println("Number of Words: " + getNumOfWords(sampleText));
					input = "";
					break;
				case "f":
					Scanner scan = new Scanner(System.in);
					System.out.println("Enter a word or phrase to be found: "); // asks user for input 
					String word = scan.nextLine(); // get the user input for which word to find
					System.out.println(); 
					System.out.println("\"" + word + "\" instances: " + findText(sampleText, word)); 
					input = ""; 
					break;
				case "r":
					System.out.println();
					System.out.println("Edited text: " + replaceExclamation(sampleText)); 
					input = "";
					break;
				case "s":
					System.out.println();
					System.out.println("Edited text: " + shortenSpace(sampleText)); 
					input = ""; 
					break;
				case "q":
					a++; // this increases the value of a so that it will no longer be less than 1 and we exit the while loop
					break;
				default:
					input = printMenu(); // calls method that prints the menu of options
					break;
			}	
		}
	}

	// Method that has the user enter a sample text
	public static String sampleText(){ 
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a sample text:");
		String text = scan.nextLine();
		System.out.println();
		System.out.print("You entered: " + text);
 System.out.println();
 return text;
	}

	 // Method that displays the menu and records the user input
	public static String printMenu(){
		System.out.println();
		System.out.println("MENU");
		System.out.println("c - Number of non-whitespace characters");
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replace all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - Quit");
		System.out.println();
		System.out.print("Choose an option: ");
		Scanner scan = new Scanner(System.in);
		String input = scan.nextLine();
		return input;
		}

	// Method that counts the number of non-whitespace characters by using a loop
	public static int getNumOfNonWSCharacters(String text){ 
		int numWhiteSpaces = 0;
		for(int i = 0; i < text.length(); i++){
			int a = 0;
			char b = text.charAt(i);
			if(b == ' '){
				a = 1;
			}
			else if(a < 1){
				numWhiteSpaces++;
			}
			a = 0;
		}
		System.out.println("");
		return numWhiteSpaces;
	}
	
	// Method that loops through the String sampleText and increments a counter for each word found
	public static int getNumOfWords(String text){ 
		int numWords = 0;
		String word = "";
		for(int i = 0; i < (text.length()-1); i++){
			if(text.charAt(i) != ' '){
				word += text.charAt(i);
				if(text.charAt(i+1) == ' '){
					numWords++;
					word = "";
				}
			}
		}
		System.out.println();
		return (numWords + 1);
	}

	// Method that loops through the String sampleText and differentiates individual words and tests if they equal the word/phrase that the user inputed
	public static int findText(String text, String wordFind){ 
		String word = "";
		int numInstance = 0;
		for(int i = 0; i < (text.length()-1); i++){
			if(text.charAt(i) != ' ' || text.charAt(i) == '.' || text.charAt(i) == ',' || text.charAt(i) == '!' || text.charAt(i) == ';' || text.charAt(i) == '?'){
				word += text.charAt(i);
				if(text.charAt(i+1) == ' ' || text.charAt(i+1) == '.' || text.charAt(i+1) == ',' || text.charAt(i+1) == '!' || text.charAt(i+1) == ';' || text.charAt(i+1) == '?'){
					if(word.equals(wordFind)){
						numInstance++;
					}
					word = "";
				}
			}
		}

		if((word + text.charAt(text.length()-1)).equals(wordFind)){
			numInstance++;
		}
		return numInstance;
	}

	// Method that takes in the String sampleText and replaces each instance of a "!" with a "."
	public static String replaceExclamation(String text){ 
		String editedText = text.replace('!','.');
		System.out.println();
		return editedText;
	}

	// Method that loops through the String sampleText and removes unnecessary whitespace
	public static String shortenSpace(String text){ 
		String word = "";
		String editedText = "";
		for(int i = 0; i < (text.length()-1); i++){
			if(text.charAt(i) != ' '){
				word += text.charAt(i);
				if(text.charAt(i+1) == ' '){
					editedText += (word + " ");
					word = "";
				}
			}
		}
		editedText += (word + text.charAt(text.length()-1));
		return editedText;	
	}
}
