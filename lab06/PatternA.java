/////////////////////
//// Pierre Klur
//// 10/12/18
//// CSE 002 - 110: Lab 06: PatternA
// The purpose of this program is to use nested loops to
// print various outputs

import java.util.Scanner; // imports the scanner

public class PatternA {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); // declares the scanner
		
    int i = 0;
		int input = 0;
		int numRows = 1;
		boolean a = false;
		
		// This while-loop gets the user input for the number of hands and makes sure it is an integer
			while (a == false){
			    //ask user for the number of hands
				System.out.println("Enter an integer between 1 and 10. ");
			    //use scanner to accept the input
			    a = myScanner.hasNextInt();
			    // if the input is the right type, this if statement assigns the value given to the variable
			         if (a == true){
			            input = myScanner.nextInt();
			            break;
			         }
			    System.out.println("You need to enter an integer");
			     //clear Scanner 
			    myScanner.next();
			 }

			for (numRows = 1; numRows <= input; numRows++) {
				for (i = 1; i <= numRows; i++) {
          System.out.print(i + " ");
        } 
        System.out.println(" ");
			}
			
	}

}