/////////////////////
//// Pierre Klur
//// 10/7/18
//// CSE 002 - 110: HW 05: While Loops
// The pupose of this porgram is to use loops to
// determine the probability of various poker hands

import java.util.Scanner; // imports the scanner

public class Hw05 {
	//main method for java documents
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); // declares the scanner
		
		// The following code initializes the variables that will be used including
		// the card values, various counters, probability values, and a boolean
		int numHands = 0;
		boolean a = false;
		int card1;
		int card2;
		int card3;
		int card4;
		int card5;
		int i = 1;
		int countOne = 0;
		int countTwo = 0;
		int countThree = 0;
		int countFour = 0;
		double probOne = 0.0;
		double probTwo = 0.0;
		double probThree = 0.0;
		double probFour = 0.0;
		int counter = 0; // use this to avoid counting "two-pair" as 2 "one-pairs"
		
		
		// This while-loop gets the user input for the number of hands and makes sure it is an integer
		while (a == false){
	        //ask user for the number of hands
			System.out.println("Enter the number of hands that should be generated. ");
	        //use scanner to accept the input
	        a = myScanner.hasNextInt();
	        // if the input is the right type, this if statement assigns the value given to the variable
	            if (a == true){
	              numHands = myScanner.nextInt();
	              break;
	            }
	            System.out.println("You need to enter an integer");
	            //clear Scanner 
	            myScanner.next();
	          }
		
		
		// The is the main for loop that will be repeated for each hand
		for (i = 1; i <= numHands; i++) {	
			
			/* The following code assigns values to the five cards in the hand
			 * and makes sure that there are no repeats by using while loops*/
			card1 = (int)(Math.random()*(52+1))+1;
			card2 = (int)(Math.random()*(52+1))+1;
			while (card2 == card1) {
				card2 = (int)(Math.random()*(52+1))+1;
				continue;
			}
			card3 = (int)(Math.random()*(52+1))+1;
			while (card3 == card2 || card3 == card1) {
				card3 = (int)(Math.random()*(52+1))+1;
				continue;
			}
			card4 = (int)(Math.random()*(52+1))+1;
			while (card4 == card3 || card4 == card2 || card4 == card1) {
				card4 = (int)(Math.random()*(52+1))+1;
				continue;
			}
			card5 = (int)(Math.random()*(52+1))+1;
			while (card5 == card4 || card5 == card3 || card5 == card2 || card5 == card1) {
				card5 = (int)(Math.random()*(52+1))+1;
				continue;
			}
			
			
			/* The following while loops convert each card to a number
			 * from 1 to 13 so that they can be compared to each other in order
			 * to evaluate pairs etc.*/
			while (card1 > 13) {
				card1 = card1 - 13;
				continue;
			}
			while (card2 > 13) {
				card2 = card2 - 13;
				continue;
			}
			while (card3 > 13) {
				card3 = card3 - 13;
				continue;
			}
			while (card4 > 13) {
				card4 = card4 - 13;
				continue;
			}
			while (card5 > 13) {
				card5 = card5 - 13;
				continue;
			}
			
			
			/////////////////////////
			// Pairs (one-pair and two-pair)
			/////////////////////////
			if (card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5) {
				countOne = countOne + 1;
				counter = counter + 1;
			}
			if (card2 == card3 || card2 == card4 || card2 == card5) {
				countOne = countOne + 1;
				counter = counter + 1;
			}
			if (card3 == card4 || card3 == card5) {
				countOne = countOne + 1;
				counter = counter + 1;
			}
			if (card4 == card5) {
				countOne = countOne + 1;
				counter = counter + 1;
			}
			// avoids counting two-pair/three of a kind/four of a kind as multiple one-pairs
			if (counter > 1) {
				countOne = countOne - counter;
			}
			// If the counter == 2, there is either a two pair or a three of a kind.
			// the code will subtract 1 from countTwo in later for the situations where the hand is actually 3 of a kind
			if (counter == 2) {
				countTwo = countTwo + 1;
			}
			counter = 0; // resets counter to 0
			
			
			/////////////////////////
			// Three of a kind
			/////////////////////////
			// 10 if statements because there are 10 possible combinations that lead to three of a kind (5C3=10)
			if (card1 == card2 && card1 == card3) {
				countThree = countThree + 1;
				countTwo = countTwo - 1; // counteracts the fact that countTwo was incorrectly increased when the hand was three of a kind
			}
			else if (card1 == card2 && card1 == card4) {
				countThree = countThree + 1;
				countTwo = countTwo - 1;
			}
			else if (card1 == card2 && card1 == card5) {
				countThree = countThree + 1;
				countTwo = countTwo - 1;
			}
			else if (card1 == card3 && card1 == card4) {
				countThree = countThree + 1;
				countTwo = countTwo - 1;
			}
			else if (card1 == card3 && card1 == card5) {
				countThree = countThree + 1;
				countTwo = countTwo - 1;
			}
			else if (card1 == card4 && card1 == card5) {
				countThree = countThree + 1;
				countTwo = countTwo - 1;
			}
			else if (card2 == card3 && card2 == card4) {
				countThree = countThree + 1;
				countTwo = countTwo - 1;
			}
			else if (card2 == card3 && card2 == card5) {
				countThree = countThree + 1;
				countTwo = countTwo - 1;
			}
			else if (card2 == card4 && card2 == card5) {
				countThree = countThree + 1;
				countTwo = countTwo - 1;
			}
			else if (card3 == card4 && card3 == card5) {
				countThree = countThree + 1;
				countTwo = countTwo - 1;
			}
			
			
			/////////////////////////
			// Four of a kind
			/////////////////////////
			// 5 if statements because there are 5 possible combinations that lead to four of a kind (5C4 = 5)
			if (card1 == card2 && card1 == card3 && card1 == card4) {
				countFour = countFour + 1;
				countThree = countThree - 1; // the code added 1 to three of a kind thinking this was 3 of a kind
				countTwo = countTwo + 1; // since the code thought this was 3 of a kind, it decreased countTwo so we have to reverse that
			}
			if (card1 == card2 && card1 == card3 && card1 == card5) {
				countFour = countFour + 1;
				countThree = countThree - 1;
				countTwo = countTwo + 1;
			}
			if (card1 == card2 && card1 == card4 && card1 == card5) {
				countFour = countFour + 1;
				countThree = countThree - 1;
				countTwo = countTwo + 1;
			}
			if (card1 == card3 && card1 == card4 && card1 == card5) {
				countFour = countFour + 1;
				countThree = countThree - 1;
				countTwo = countTwo + 1;
			}
			if (card2 == card3 && card2 == card4 && card2 == card5) {
				countFour = countFour + 1;
				countThree = countThree - 1;
				countTwo = countTwo + 1;
			}
			
		} // end of for-loop
		
		
		// Calculate the probability of each hand
        probOne = (double) countOne / (double) numHands;
        probTwo = (double) countTwo / (double) numHands;
        probThree = (double) countThree / (double) numHands;
        probFour = (double) countFour / (double) numHands;
        
        
        // Prints the final results
        System.out.println("The number of loops: " + numHands);
        System.out.printf("The probability of Four-of-a-kind: %.4f \n" , probFour); // I changed this to 4 decimal places because the true probability of 4 of a kind is 0.00024 
        System.out.printf("The probability of Three-of-a-kind: %.3f \n" , probThree);
        System.out.printf("The probability of Two-pair: %.3f \n" , probTwo);
        System.out.printf("The probability of One-pair: %.3f \n" , probOne);
	} 
//closes the main method
}